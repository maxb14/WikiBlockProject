pragma solidity ^0.4.2;

import "truffle/Assert.sol";
import "truffle/DeployedAddresses.sol";
import "../contracts/TokenERC20.sol";

contract TestContracts {

  function testDeployedContract() {
    TokenERC20 token = TokenERC20(DeployedAddresses.TokenERC20());

    uint expected = 100;

    Assert.equal(token.getBalance(tx.origin), expected, "Adresse 1 : 100 ethers !");
  }

  function testTokenERC20() {
    TokenERC20 token = new TokenERC20();

    uint expected = 100;

    Assert.equal(token.getBalance(tx.origin), expected, "Adresse 1 : 100 ethers !");
  }
}
