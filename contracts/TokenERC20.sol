pragma solidity ^0.4.2;

/**
 * Simple token ERC20 pour gérer le flux de monnaie sur l'application
 * Méthodes implémentées: transfer(), withdraw(), getBalance()
 * Possibilité d'ajouter de nombreuses autres méthodes pour améliorer le contrat
 *
 * Les require permettent de gérer les execptions
 */
contract TokenERC20 {
	
	mapping (address => uint) public balances;
	uint public totalSupply;
	
	function TokenERC20() public {
		totalSupply = 100 ether;
		balances[msg.sender] = totalSupply;
	}
	
	function() public payable {
		totalSupply += msg.value;
		balances[msg.sender] += msg.value;
	}
	
	function withdraw(uint value) public {
		require(balances[msg.sender] >= value);
		balances[msg.sender] -= value;
		totalSupply -= value;
		require(msg.sender.send(value));
	}
	
	function transfer(address to) public payable returns (bool success) {
		require(balances[msg.sender] >= msg.value);
		balances[to] += msg.value;
		balances[msg.sender] -= msg.value;
		return true;
	}

	function getBalanceEth() public returns (uint) {
		return balances[msg.sender];
	}
}