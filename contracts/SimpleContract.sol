pragma solidity ^0.4.2;

/**
 * Simple contrat executé dans le blockchain locale pour afficher un texte.
 */
contract SimpleContract {
	function print() public returns (string) {
		return ("Ceci est un contrat déployé dans notre blockchain !");
	}
}