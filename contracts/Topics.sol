pragma solidity ^0.4.2;

/**
 * Contrat topic pour gérer les création d'un nouveau message avec 3 paramètres :
 * > id, adresse auteur, titre, contenu
 *
 * Une fonction permet de récupérer un message en fonction de son identifiant
 * 
 * Dans ce contrat, on peut ajouter un prix quand quelqu'un envoie un message
 * il suffit de modifier la variable currentPostPrice
 */
contract Topics {

	string public title;
	string public message;
	address public currentPoster = msg.sender;
	uint public currentPostPrice = 0;

	function getMessage() public returns (string, string) {
		return (title, message);
	}

	function setMessage(string _title, string _message) public payable {
		require(msg.value > currentPostPrice);
		require(currentPoster.send(currentPostPrice));
		title = _title;
		message = _message;
		currentPoster = msg.sender;
		currentPostPrice = msg.value;
	}
}

