require('babel-register')

/**
 * Configuration du réseau blockchain sur lequel l'application doit se connecter
 */
module.exports = {
  networks: {
    development: {
      host: '127.0.0.1',
      port: 7545, // Port Ganache
      network_id: '*' 
    }
  }
}
