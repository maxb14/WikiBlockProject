#############################################
#											#
#				WikiBlock					#
#											#
#	Decrentalized wiki made with Ethereum	#
#											#
#				Maxime Blaszka				#
#				M2 DNR2i					#
#				2017/2018					#
#											#
#############################################

Pré-requis: 

> truffle
> Ganache
> nodeJS
> Plugin Chrome MetaMask

Lancer l'application:

0) Initialiser les modules nodes si pas fait
> npm install

1) Ouvrir un shell à la racine du projet
> npm run dev

2) Lancer Ganache

3) Compiler les contrats
> truffle compile

4) Migrer les contrats sur la blockchain
> truffle migrate

6) Configurer MetaMask dans votre navigateur sur la blockchain Ganache
> HTTP://127.0.0.1:7545

7) Rendez-vous à l'adresse de l'application dans votre navigateur
> http://localhost:8080/

-------------------------

Note: Pour débugger certaines erreurs, allez dans les paramètres du plugin metamask puis cliquez sur "reset account"
