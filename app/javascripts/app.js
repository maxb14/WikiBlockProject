import "../stylesheets/app.css";

// Import libraries
import { default as Web3} from 'web3';
import { default as contract } from 'truffle-contract';

import token_artifacts from '../../build/contracts/TokenERC20.json';
import topic_artifacts from '../../build/contracts/Topics.json';

// On récupère nos contrat compilé
var TokenERC20 = contract(token_artifacts);
var Topics = contract(topic_artifacts);

// Variables globales
var accounts;
var account;
var balanceth;

window.App = {
  start: function() {
    var self = this;

    // On initialise notre contrat avec le provider courrant
    TokenERC20.setProvider(web3.currentProvider);
    Topics.setProvider(web3.currentProvider);

    // On récupère tous les comptes de notre réseau
    web3.eth.getAccounts(function(err, accs) {
      // Ici on gère simplement de l'affichage
      if (err != null || accs.length == 0) {
        var alert = document.getElementById("alert");
        var topic = document.getElementById("topicname");
        var message = document.getElementById("message");
        var amount = document.getElementById("amount");
        var receiver = document.getElementById("receiver");
        topic.setAttribute("disabled", "disabled");
        message.setAttribute("disabled", "disabled");
        amount.setAttribute("disabled", "disabled");
        receiver.setAttribute("disabled", "disabled");
        alert.setAttribute("class", "alert alert-danger alert-dismissible");
        alert.innerHTML = 'Compte ethereum non trouvé ! Connectez-vous à une blockchain avec MetaMask <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        return;
      }

      accounts = accs;
      account = accounts[0];  

    });
    self.refreshBalance();
  },
  setStatus: function(message) {
    var status = document.getElementById("status");
    status.innerHTML = message;
  },
  refreshBalance: function() {
    var self = this;
    var token;
    var balance_element = document.getElementById("balance");
    var accountaddress = document.getElementById("accountaddress");
    var account = document.getElementById("account");

    // On vérifie si on récupère bien la balance de l'user dans notre contrat
    TokenERC20.deployed().then(function(instance) {
      token = instance;
      // On récupère la balance de l'utilisateur courrant
      return new Promise (function (resolve, reject) {
        web3.eth.getBalance(accounts[0], function (error, result) {
          if (error) {
            reject(error);
          } else {
            resolve(result);
            balanceth = web3.fromWei(result, 'ether').toFormat(2);
          }
        })
      });
    }).then(function(value) {
      // Si c'est ok on affiche les données du compte utilisateur
      account.setAttribute("value", accounts[0]);
      accountaddress.innerHTML = "<i>"+accounts[0]+"</i>";
      balance_element.innerHTML = "<b>"+balanceth+"</b> ETH";
    }).catch(function(e) {
      // Sinon on affiche un message d'erreur
      accountaddress.innerHTML = "Vous n'êtes pas connecté";
      balance_element.innerHTML = "";
      self.setStatus("Impossible de récuperer votre balance, vérifiez la connexion avec MetaMask.");
    });
  },
  createMessage: function() {
    var topic;
    var errorsend = document.getElementById("errorsend");
    var topics = document.getElementById("topics");
    var topicname = document.getElementById("topicname").value;
    var message = document.getElementById("message").value;

    Topics.deployed().then(function(instance) {
      topic = instance;

      // On enregistre le message dans la blockchain, pour faire un test on met un prix de 1 ether par message
      return topic.setMessage(topicname, message, {from: accounts[0], value: web3.toWei(1, 'ether')});
    }).then(function() {
      var getMessage = topic.message();
      var getMessageFunction = topic.getMessage({from: accounts[0]});

      getMessageFunction.then(function(result) {
        console.log(result);
        topics.innerHTML += "<div class=\"panel panel-info\"><div class=\"panel-heading\">"+getMessage.title+" - posted by "+account+"</div><div class=\"panel-body\">"+getMessage.message+"</div></div>";
      })
      .catch(function(e) {
        console.log(e);
      });

      if (topicname == "" || message == "") {
        errorsend.innerHTML = "Veuillez remplir tous les champs !";
      } 
      else {  // Si le topic est bien enregistré dans la blockchain on l'affiche en front pour tester le visuel
        errorsend.innerHTML = "";
        topics.innerHTML += "<div class=\"panel panel-info\"><div class=\"panel-heading\">"+topicname+" - posted by "+account+"</div><div class=\"panel-body\">"+message+"</div></div>";
      }
    }).catch(function(e) {
      console.log(e);
    });
  },
  sendEther: function() {
    var self = this;
    var amount = parseInt(document.getElementById("amount").value);
    var receiver = document.getElementById("receiver").value;
    var statusend = document.getElementById("statusend");
    var token;

    console.log(web3.currentProvider);
    TokenERC20.deployed().then(function(instance) {
      token = instance;

      /**
       * Méthode 1 : Transfert d'ethers par le biais du contrat TokenERC20
       * On précise l'adresse du receveur et les options d'envoi + on fixe un gas price limit (fix bug)
       */
      statusend.innerHTML = "Initialisation de la transaction...";
      return token.transfer(receiver, {from: accounts[0], value: web3.toWei(amount, 'ether'), gas: 4000000}) &&

      // Méthode 2 : Transfert d'ethers par le biais de web3 dans la blockchain locale avec Ganache
      web3.eth.sendTransaction({from: accounts[0], to: receiver, value: web3.toWei(amount, 'ether')}, function(error, result){
        if (error) {
            console.error("Transaction impossible !"+error);
            statusend.innerHTML = "Transaction impossible !";
        }
      });
    }).then(function() {
      statusend.innerHTML = "Transaction réussie !";
      self.refreshBalance();
    }).catch(function(e) {
      console.log(e);
      statusend.innerHTML = "Impossible d'envoyer les ethers, adresse ou montant invalide !";
    });
  }
};

/**
 *  On execute ceci au début du chargement de la page web
 *  On va vérifier si un compte est trouvé puis appeler la méthode start()
 */
window.addEventListener('load', function() {
  // On check si l'utilisateur est bien econnecté à une blockchain
  if (typeof web3 !== 'undefined') {
    window.web3 = new Web3(web3.currentProvider);
  } else {
    console.log("Compte ethereum non trouvé ! Utilisez la blockchain test http://localhost:7545.");
    // On essaye de forcer la connexion
    window.web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));
  }
  App.start();
});