/**
 * Dans ce fichier on spécifie les contrats que l'on souhaite déployer sur la blockchain
 */
var TokenERC20 = artifacts.require("./TokenERC20.sol");
var SimpleContract = artifacts.require("./SimpleContract.sol");
var Topics = artifacts.require("./Topics.sol");

module.exports = function(deployer) {
  deployer.deploy(SimpleContract);
  deployer.deploy(TokenERC20);
  deployer.deploy(Topics);
};
